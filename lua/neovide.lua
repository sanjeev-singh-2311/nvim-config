-- Config for neovide 
-- Set the font to Iosevka
vim.o.guifont = 'Iosevka_Term_Medium_Extended:h10'

-- Start in full screen
vim.g.neovide_fullscreen = false

-- Disable cursor animations to increase performace
vim.g.neovide_cursor_animation_length = 0.1

vim.g.neovide_transparency = 0.95

