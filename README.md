# Config file for neovim

## Requirements
- Install [packer.nvim](https://github.com/wbthomason/packer.nvim#quickstart) for your OS.
- Install a [Nerd Font](https://www.nerdfonts.com/).

## Installation
- Unix and Linux:
`git clone https://gitlab.com/Yttrium_32/nvim-config.git ~/.config/nvim`

- Windows(Using git-bash):
`git clone https://gitlab.com/Yttrium_32/nvim-config.git ~/.config/nvim`

Then in nvim run `:PackerInstall` to install all plugins and then relaunch nvim.



